#!/bin/bash
# This script is heavily inspired by the one on NERSC Cori: /usr/common/software/ParaView/start_pvserver.sh

if [ $# != 7 ]
then
  echo "expected 7 argumnets but got $#"
  echo "Please upgrade your login configuration: https://scicomp.ethz.ch/public/paraview/euler.pvsc"
  echo "args:"
  echo $*
  echo
  echo "Usage: start_pvserver.sh"
  echo
  echo "  NNODES          - Number of nodes"
  echo "  NCORES_PER_NODE - Number of cores per node"
  echo "  MEM_PER_CORE_MB - Memory per core (MB)"
  echo "  WALLTIME        - wall time in HH:MM:SS format."
  echo "  ACCOUNT         - account name to run the job against."
  echo "  PORT            - the port number of the server side tunnel."
  echo "  PV_VER          - the desired pv version number."
  echo
  echo "assumes a reverse tunel is set up on localhost to the remote site."
  echo
  sleep 1d
fi
export EULER_HOST=euler
export PARAVIEW_DIR=/cluster/apps/nss/paraview

NNODES=$1
NCORES_PER_NODE=$2
MEM_PER_CORE_MB=$3
WALLTIME=$4
ACCOUNT=$5
PORT=$6
PV_VER=$7

# this is the recommended version to use on Euler
# when new PV is installed this value needs to be updated
EULER_PV_VER=5.9.1
if [[ "$PV_VER" != "$EULER_PV_VER" ]]
then
  echo\
    "WARNING: You're using ParaView ver. $PV_VER. The recommended "\
    "version is ParaView ver. $EULER_PV_VER"
fi

echo `ls -1 $PARAVIEW_DIR | grep "[3456].[0-9].[0-9]"`
echo `pwd`

if [[ ! (-e $PARAVIEW_DIR/$PV_VER/start_pvserver.sh) ]]
then
  PV_INSTALLS=`ls -1 $PARAVIEW_DIR | grep "[0-9].[0-9].[0-9]$" | tr '\n' ' '`

  echo
  echo\
    "Error: Version $PV_VER is not installed. Client and server "\
    "versions must match. You will need to download and install a client "\
    "binary for one of the following installed versions from www.paraview.org "\
    "and try again."
  echo
  echo $PV_INSTALLS
  echo
  echo "$PARAVIEW_DIR/$PV_VER/start_pvserver.sh"

  sleep 1d
fi

$PARAVIEW_DIR/$PV_VER/start_pvserver.sh $NNODES $NCORES_PER_NODE $MEM_PER_CORE_MB $WALLTIME $ACCOUNT $PORT
echo "Job has exited. Goodbye!"
sleep 15s
