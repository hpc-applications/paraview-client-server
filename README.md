# Paraview-client-server

This repository contains all the scripts required to setup the client-server mode of paraview.

## Installation

### User script

As your own account, run `./install_user.sh`.

### Server script
You will need to login as the apps account before running the commands.

Here I am assuming that you wish to install version 5.9.1.
First you need to install paraview in the usual directory (`/cluster/apps/nss/paraview/5.9.1` using the version number as the directory is important for the scripts).
Then you will need to update `start_pvserver.sh` and update `EULER_PV_VER`.
Finally, you can install the scripts with `./install.sh 5.9.1` on euler (operational nodes).

## Quick description of the scripts
To setup everything, the user needs to setup the server configuration (`euler.pvsc`).
This script will call a first script on the server `start_pvserver.sh` which mainly pick the correct paraview version.
Once the version found, it calls `start_pvserver-VERSION.sh` which setups all the environment variables and run the job.
The file `start_pvserver.qsub` is the script submitted to the queue.
Finally, `batchsysmon.sh` is a simple interactive shell that allows the user to deal with the queue while waiting for the job to run.


## Known limitations

Currently the script is not working with multiple process (only 1 process with multiple threads). This is due to the ssh tunnel from the compute node. To solve this, we would need to do the tunnel only between the master node and the login node.

There is an issue with the ssh tunnel between Euler-8 and the login nodes. For now, we simply use Euler-7
