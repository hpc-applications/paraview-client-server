#!/bin/bash

set -e

WIKI_DIR=/nfs/nas22.ethz.ch/fs2201/id_sis_hpc_group/wiki

mkdir -p $WIKI_DIR/public/paraview/
cp euler.pvsc $WIKI_DIR/public/paraview/
chmod 775 $WIKI_DIR/public/paraview
chmod 644 $WIKI_DIR/public/paraview/euler.pvsc
