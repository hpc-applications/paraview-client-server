#!/bin/bash -l
# This script is heavily inspired by the one on NERSC Cori: /usr/common/software/ParaView/5.10.0/start_pvserver.sh

if [ $# != 6 ]
then
  echo "Please upgrade your login configuration: https://scicomp.ethz.ch/public/paraview/euler.pvsc"
  echo "Usage: start_pvserver.sh NCPUS NCPUS_PER_SOCKET WALLTIME ACCOUNT PORT"
  echo
  echo "  NNODES          - Number of nodes"
  echo "  NCORES_PER_NODE - Number of cores per node"
  echo "  MEM_PER_CORE_MB - Memory per core (MB)"
  echo "  WALLTIME        - wall time in HH:MM:SS format."
  echo "  ACCOUNT         - account name to run the job against."
  echo "  PORT            - the port number of the server side tunnel."
  echo
  echo "assumes a reverse tunel is set up on localhost to the remote site."
  echo
  sleep 1d
fi

NNODES=$1
NCORES_PER_NODE=$2
MEM_PER_CORE_MB=$3
WALLTIME=$4
ACCOUNT=$5
PORT=$6

LOGIN_HOST=`hostname -I | grep -o -P '\d+\.\d+\.\d+\.\d+' | tail -n 1`

PV_VER_SHORT=PARAVIEW_VERSION_SHORT
PV_VER_FULL=PARAVIEW_VERSION_FULL
PV_HOME=$PARAVIEW_DIR/$PV_VER_FULL/

# Load the required module
module load paraview/${PV_VER_FULL}_headless

export PV_LD=$PV_HOME/lib64:$PV_HOME/lib64/paraview-$PV_VER_SHORT
export PV_PATH=$PV_HOME/bin
export LOGIN_PORT=$PORT

echo '======================================='
echo '   ___               _   ___           '
echo '  / _ \___ ________ | | / (_)__ _    __'
echo ' / ___/ _ `/ __/ _ `/ |/ / / -_) |/|/ /'
echo '/_/   \_,_/_/  \_,_/|___/_/\__/|__,__/ '
echo
echo '======================================='
echo
echo "Version $PV_VER_FULL"

echo "Please be patient, it may take some time for the job to pass through the queue."
echo "KEEP THIS TERMINAL OPEN WHILE USING PARAVIEW"
echo
echo "Setting environment..."
echo "LD_LIBRARY_PATH=$LD_LIBRARY_PATH"
echo "PATH=$PATH"
echo "NNODES=$NNODES"
echo "NCORES_PER_NODE=$NCORES_PER_NODE"
echo "MEM_PER_CORE_MB=$MEM_PER_CORE_MB"
echo "WALLTIME=$WALLTIME"
echo "HOST=$(hostname)"
echo "PORT=$PORT"
echo "ACCOUNT=$ACCOUNT"
echo "QOS=$QOS"
echo "LOGIN_HOST=${LOGIN_HOST}"
echo "LOGIN_PORT=$LOGIN_PORT"
echo "LINK_TYPE=static"

echo "Starting ParaView via qsub..."
# pass these to the script
export PV_HOME
export PV_LD
export PV_PATH
export PV_PORT=$PORT
export PV_NODES=$NNODES
export PV_NCORES_PER_NODE=$NCORES_PER_NODE
export PV_MEM_PER_CORE_MB=$MEM_PER_CORE_MB
export PV_LOGIN_HOST=${LOGIN_HOST}
export PV_LOGIN_PORT=$LOGIN_PORT
export ATP_ENABLED=1
OPTIONS="-J ParaView-$PV_VER_FULL -q \"$QUEUE\" --nodes=$NNODES --ntasks=$NCORES_PER_NODE --mem-per-cpu=${MEM_PER_CORE_MB}MB -t $WALLTIME --constraint=ib"
if [[ "$ACCOUNT" != "default" ]]
then
    OPTIONS="$OPTIONS -A \"$ACCOUNT\""
fi
JID=`sbatch $OPTIONS $PV_HOME/start_pvserver.qsub`

ERRNO=$?
if [ $ERRNO == 0 ]
then
echo "Job submitted succesfully."
else
echo "ERROR $ERRNO: in job submission."
fi

# monitor the batch system and provide
# a simple UI for probing job status
JIDNO="${JID//[!0-9]/}"
$PARAVIEW_DIR/batchsysmon.sh $JIDNO slurm-${JIDNO}.out
