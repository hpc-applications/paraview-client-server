#!/bin/bash

set -e

# If changed, this value needs also to be changed within the scripts
PARAVIEW_DIR=/cluster/apps/nss/paraview

if [ $# != 1 ]
then
   echo You forgot to provide the version.
   exit 1
fi

# Get the version
VERSION=$1
SHORT_VERSION=${VERSION%.*}
echo "Install version $SHORT_VERSION (full version: $VERSION)"


echo Update the queue manager
cp batchsysmon.sh $PARAVIEW_DIR/
chmod 775 $PARAVIEW_DIR/batchsysmon.sh


echo Update the main script
cp start_pvserver.sh $PARAVIEW_DIR/
chmod 775 $PARAVIEW_DIR/start_pvserver.sh


echo Copy the script for the specific version
cp start_pvserver-VERSION.sh $PARAVIEW_DIR/$VERSION/start_pvserver.sh
chmod 775 $PARAVIEW_DIR/$VERSION/start_pvserver.sh


echo Update the version within the previous script
sed -i "s/PARAVIEW_VERSION_SHORT/$SHORT_VERSION/g" $PARAVIEW_DIR/$VERSION/start_pvserver.sh
sed -i "s/PARAVIEW_VERSION_FULL/$VERSION/g" $PARAVIEW_DIR/$VERSION/start_pvserver.sh


echo Copy qsub
cp start_pvserver.qsub $PARAVIEW_DIR/$VERSION/
chmod 775 $PARAVIEW_DIR/$VERSION/start_pvserver.qsub
